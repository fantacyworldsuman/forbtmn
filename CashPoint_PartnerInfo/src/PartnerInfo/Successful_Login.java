package PartnerInfo;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Successful_Login{
		 
	protected static WebDriver cp;
	
    @BeforeClass
    public static void Driver_Creation() throws InterruptedException {
    	//System.setProperty("webdriver.chrome.driver", "E:\\Suman\\Softwares\\Cell\\ChromeDriver\\chromedriver.exe");
		cp = new FirefoxDriver();
		cp.get("https://pi.cashpoint.com/");
		Thread.sleep(4000);	

		//cp.findElement(By.id("username")).sendKeys("pitest1");
		//cp.findElement(By.id("password")).sendKeys("$Pitest1");
		//cp.findElement(By.id("submit")).click();  Thread.sleep(5000);
    }
	
	@Test(priority =0)
	public void Username_Visibility_Check(){
		//cp.findElement(By.id("username")).click();
		cp.findElement(By.id("username")).sendKeys("pitest1");
		if(cp.findElement(By.id("username")).isDisplayed())
			System.out.println("The User name is visible" +"\n");
		else
			System.out.println("The USer name isn't visible" +"\n");
	}
	
	@Test(priority =1)
	public void Password_Visibility_Check(){
		cp.findElement(By.id("password")).click();
		cp.findElement(By.id("password")).sendKeys("$Pitest1");
		WebElement pwd=cp.findElement(By.id("password"));
		boolean isEncrypted = pwd.getAttribute("type").equals("password");
		if(isEncrypted == true)
			{
			if(cp.findElement(By.id("password")).isDisplayed())
				System.out.println("The Password is encrypted and visible." +"\n");
			else
				System.out.println("The Password is encrypted but not visible." +"\n");
			}
		else
			System.out.println("The Password isn't encrypted.");		
	}
	
	@Test(priority =2)
	public void Login_button_Highlight(){
		WebElement ele = cp.findElement(By.id("submit"));
		Actions ac = new Actions(cp);
		
		String btn_color = cp.findElement(By.id("submit")).getCssValue("background-color");
		System.out.println("The Login button code in rgba format is: " + btn_color);
		System.out.println("The Login button code in Hex format is: " + Color.fromString(ele.getCssValue("background-color")).asHex() + "\n");
		
		ac.moveToElement(ele).perform();
		String btn_hovered_color = cp.findElement(By.id("submit")).getCssValue("background-color");
		System.out.println("After hovering the Login button code in rgba format is: " + btn_hovered_color);
		System.out.println("After hovering the Login button code in Hex format is: " + Color.fromString(ele.getCssValue("background-color")).asHex() + "\n"); 

		
		if(btn_hovered_color.equals(btn_color))
			System.out.println("Button color isn't getting changed and it's code is : "+ btn_color +"\n\n");
		else
			System.out.println("Button color is getting changed and it's code is : "+ btn_hovered_color +"\n");		
	}
	
	@Test(priority =3)
	public void Login() throws InterruptedException{
		cp.findElement(By.id("submit")).click();		
		Thread.sleep(5000);
		String S1= cp.findElement(By.xpath("//div[@id='user_info']/ul/li[3]")).getText();
		if(S1.equals("User: pitest1"))
			System.out.println("The username matches and it is "+ S1 +"\n");
		else
			System.out.println("The Username doesn't match.");				
		Point str2= cp.findElement(By.id("logout")).getLocation();
		String s2= cp.findElement(By.id("logout")).getText();		
		if(s2.equals("Logout"))
			System.out.println("The logout text has been found and it's position is :" + str2);
		else
			System.out.println("Display the proper text.");
		System.out.println("\n"+"\n"+"-------------The Login Test Case is finished and now the Logout Test Case will start.------------"+"\n"+"\n");
				
	}
	
	/*@Test(priority =5)
	public void Back_button() throws InterruptedException{
		//WebDriver cp2=new FirefoxDriver();
		//cp2.get("https://pi.cashpoint.com");
		//cp2.findElement(By.id("username")).sendKeys("pitest1");
		//cp2.findElement(By.id("password")).sendKeys("$Pitest1");
		//String lnk1 = cp.getCurrentUrl();
		cp.findElement(By.id("submit")).click();
		Thread.sleep(3000);
		cp.navigate().back();
		String lnk2 = cp.getCurrentUrl();
		if (lnk1.equals(lnk2))
		  System.out.println("The link is navigated to the exact url and it is :" + lnk2 +"\n\n");
		else
		  System.out.println("Sorry the link doesn't match and the current navigated url is : " + lnk2 +"\n\n");
		Thread.sleep(1000);
		cp.quit();
		}
	
	/*@AfterClass
	public void DriverExit(){
		cp.quit();
		
	}*/
}
